import json
import requests

from bs4 import BeautifulSoup
from dateutil.parser import parse

LIVEPAGE = False  # should we load from live page or cached page
out_path = "./memories-oakley-cook.json"  # output file for results

# get the data
# note: "save as" from browser, because comments do not load via `requests.get()`
if LIVEPAGE:
    data = requests.get("https://www.oakley-cook.com/obituaries/Stanley-Bowman/")
    data = data.text
else:
    filename = "./oakley-cook-website/Stanley Keith Bowman Obituary - Visitation & Funeral Information.html"
    data = open(filename, encoding="utf8")

# load data into bs4
soup = BeautifulSoup(data, "html.parser")

# find all comments and signatures
comments = [
    comment.text.strip() for comment in soup.find_all("p", {"class": "emojified"})
]

signatures = [
    sig.text.strip() for sig in soup.find_all("div", {"class": "comment-sent"})
]

# let us know if the comment count doesn't match the signature count
assert len(comments) == len(signatures)

# join them together into single comments
sig_split = " - "
memories = sorted(
    [
        {
            "name": pair[1].split(sig_split)[0].strip(),
            "date": pair[1].split(sig_split)[1].strip(),
            "comment": pair[0],
        }
        for pair in zip(comments, signatures)
    ],
    key=lambda x: parse(x["date"]),
)

# save to a file that will be checked into the repo
with open(out_path, "w") as memoryfile:
    json.dump(memories, memoryfile)

# print to output in a friendly format
for memory in memories:
    print("--------------------------------------------------------------\n")
    print(f'{memory["comment"]}\n\n - {memory["name"]} ({memory["date"]})\n')
print(f"Total: {len(memories)}")
