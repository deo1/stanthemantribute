import json
from collections import defaultdict
from itertools import chain
from os import path

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from PIL import Image
from wordcloud import ImageColorGenerator, WordCloud

from util import drop_sub_gram, final_filter, ngram, tokenize

out_path = "./memories-oakley-cook.json"
with open(out_path, "r") as memoryfile:
    memories = json.load(memoryfile)

word_counts = defaultdict(int)
comments = [tokenize(m["comment"]) for m in memories]

# word counts
for comment in comments:
    for n in reversed(range(1, 8)):
        for word in ngram(comment, n):
            word_counts[word] += 1

# filter
grams = list(word_counts.keys())
n = len(grams)
for i in range(n):
    for j in range(i + 1, n):
        wordl, wordr = grams[i], grams[j]
        if drop_sub_gram(wordl, wordr, word_counts):
            del word_counts[wordl]
        if drop_sub_gram(wordr, wordl, word_counts):
            del word_counts[wordr]

word_counts = final_filter(word_counts)
word_counts = sorted(word_counts.items(), key=lambda x: x[1], reverse=True)

print("\n".join([f"{w} ({c})" for w, c in reversed(word_counts)]))
print(f"\ntotal grams: {len(word_counts)}")


# make word cloud image
strat = np.array(Image.open(path.join("./", "fender_strat.jpg")))
image_colors = ImageColorGenerator(strat)
wc = WordCloud(
    background_color="white",
    max_words=10 ** 6,
    mask=strat,
    stopwords=set(),
    max_font_size=124,
    random_state=1953_2018,
    contour_width=3,
    contour_color="oldlace",  # https://stackoverflow.com/questions/22408237/named-colors-in-matplotlib
    collocations=False,
)

# generate word cloud
words_flat = ",".join(chain(*[[w for _ in range(c)] for w, c in word_counts]))
wc.generate(words_flat)
# wc.generate_from_frequencies(dict(word_counts))  # doesn't retain word ngram groupings

# create coloring from image
image_colors = ImageColorGenerator(strat)

# show
fig, ax = plt.subplots(1, 1, figsize=(36, 20))
ax.imshow(wc.recolor(color_func=image_colors), interpolation="bilinear")
plt.axis("off", bbox_inches="tight")
fig.savefig("strat_word_cloud.png")
plt.show()
