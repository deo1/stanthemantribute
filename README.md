# Stan the Man Tribute

We love you, Dad.

*Stanley Keith Bowman. 1953 - 2018*

![wordcloud](./strat_word_cloud_crop_edit.png)

## Setup

1. Install [Miniconda 3.x](https://conda.io/en/latest/miniconda.html)
2. Create a **stan** conda python 3.7 environment with all dependencies
    - `conda env create -f environment.yml`
    - `conda activate stan`
3. Making changes
    - `python -m black .`
    - `python -m pytest`

## Steps

1. Save the [website]("https://www.oakley-cook.com/obituaries/Stanley-Bowman/") from within a browser to the folder `./oakley-cook-website/`
2. Run `oakleycook.py`
3. Run `make_wordcloud.py`
