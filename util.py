from typing import Dict, List, Iterable

from nltk import download
from nltk.corpus import stopwords

download("stopwords", quiet=True)
stop_words = set(stopwords.words("english"))
stop_words.add("i")
stop_words.add("get")
stop_words.add("im")
stop_words.add("cant")
stop_words.add("can't")
stop_words.add("wont")
stop_words.add("won't")
stop_words.add("cannot")
stop_words.add("werent")
stop_words.add("weren't")
stop_words.add("would")
stop_words.add("came")
stop_words.add("could")
stop_words.add("ive")
stop_words.add("i've")
stop_words.add("g")
stop_words.add("diid")
stop_words.add("haha")
stop_words.add("hey")
stop_words.add("hi")
stop_words.add("enemy")
stop_words.add("wrong")


def final_filter(word_counts: Dict[str, int]) -> Dict[str, int]:
    for g in []:
        del word_counts[g]
    return word_counts


def sanitize_word(word: str) -> str:
    return (
        word.strip()
        .lower()
        # .replace("'", "")
        .replace('"', "")
        .replace("’", "")
        .replace("(", "")
        .replace(")", "")
        .replace("“", "")
        .replace("”", "")
        .replace("jessee", "jesse")
        .replace("allie", "ally")
        .replace("zack", "zach")
        .replace("pamchuck", "pam. chuck tipton")
    )


def sub_gram(gram: str, big_gram: str) -> bool:
    return gram in big_gram and gram != big_gram


def drop_sub_gram(gram: str, big_gram: str, word_counts: Dict[str, int]) -> bool:
    return (
        sub_gram(gram, big_gram)
        and gram in word_counts
        and big_gram in word_counts
        and word_counts[gram] <= word_counts[big_gram]
    )


def tokenize(comment: str) -> List[str]:
    tokens: List[str] = []
    for word1 in comment.replace("&", " and ").split():
        for word2 in word1.split("-"):
            word = sanitize_word(word2)
            if word:
                tokens.append(word)
    return tokens


punc = [",", ".", "!"]


def ngram(tokens: str, n: int) -> List[str]:
    ngrams: List[str] = []
    for ii in range(n, len(tokens) + 1):
        gram = tokens[ii - n : ii]
        if all([g not in stop_words for g in gram]):
            joined = " ".join(gram)
            stripped = joined
            for p in punc:
                stripped = stripped.rstrip(p)
            if all([p not in stripped for p in punc]):
                ngrams.append(stripped)
    return ngrams
